import task5250.order;
import java.util.ArrayList;
import java.util.Date;
public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");
        ArrayList<order> arrayList = new ArrayList<>();

        order order0 = new order();
        order order1 = new order(323122);
        order order2 = new order(21231232, "cuong" , 223344440);
        order order3 = new order(342324324, "nguyen", 3000000 , new Date() ,true , new String[] {"tủ lạnh"});

        arrayList.add(order0);
        arrayList.add(order1);
        arrayList.add(order2);
        arrayList.add(order3);

        for (order order : arrayList) {
            System.out.println(order.toString());
        }
    }
}

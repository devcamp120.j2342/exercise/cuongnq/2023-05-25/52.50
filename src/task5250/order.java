package task5250;
import java.util.Arrays;
import java.util.Date;

public class order {
    int id; // id của order
    String customerName; // tên khách hàng
    long price;// tổng giá tiền
    Date orderDate; // ngày thực hiện order
    boolean confirm; // đã xác nhận hay chưa?
    String[] items; // danh sách mặt hàng đã mua
    public order(int id, String customerName, long price, Date orderDate, boolean confirm, String[] items) {
        this.id = id;
        this.customerName = customerName;
        this.price = price;
        this.orderDate = orderDate;
        this.confirm = confirm;
        this.items = items;
    }
    public order(int id) {
        this.id = id;
        this.customerName = "cuong";
        this.price = 200000000;
        this.orderDate = new Date();
        this.confirm = true;
        this.items = new String[] {"máy giặt" , "tủ lạnh" , "khăn tắm"};

    }
    public order(int id, String customerName, long price) {
        this(id, customerName , price , new Date(), true ,new String[] {"máy giặt" , "tủ lạnh" , "khăn tắm"} );

    }
    public order() {
        this(322323);
    }

    @Override
    public String toString() {
        return "Person{" + "id=" + id + ", customerName=" + customerName + ", price=" + price + ", orderDate=" + orderDate + ", confirm=" + confirm + ", items=" + Arrays.toString(items) +'}';
    }

    

    


    
}
